# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_lebt-010_vac-ioc-daq001_PATH
# @runtime YES

# @field IOC
# @runtime YES

#Load the customizing database that contains default values in case autosave files are missing
dbLoadRecords("lebt-010_vac-ioc-daq001.db")

#Load the customizing database that controls VVMC operation modes
dbLoadRecords("lebt-010_vac-ioc-daq001_vvmc.db")

#Load iocStats records
dbLoadTemplate(iocAdminSoft.substitutions, "IOC=$(IOC=LEBT-010:Vac-IOC-DAQ001)")

requireSnippet(recSyncSetup.cmd)

# Configure autosave
# Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_lebt-010_vac-ioc-daq001_PATH)", "misc")

# Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
set_pass0_restoreFile("lebt-010_vac-ioc-daq001.sav")

# Create monitor set
doAfterIocInit("create_monitor_set('lebt-010_vac-ioc-daq001.req', 1, '')")

